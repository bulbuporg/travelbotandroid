package com.travelbot.travelbot.activiti;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.adapter.dongchatadapter;
import com.travelbot.travelbot.adapter.dongchatbotadapter;

import com.travelbot.travelbot.mode.Chatitem;
import com.travelbot.travelbot.mode.HInhitem;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{

    ImageButton nutchat,guihinh,guilink,guigif,nutchupanh;
    ListView listchat,listgoiy;
    dongchatadapter dongchatadapter;
    ArrayList<Chatitem> arrchat;
    String lanoidungchat;
    EditText noidungchatedittext;
    ImageView hinhchat;

    dongchatbotadapter dongchatbotadapter;
    ArrayList<Chatitem> arrchatbot;




    @Override
    protected void onCreate(Bundle savedInstanceState)
        {
          super.onCreate(savedInstanceState);
          setContentView(R.layout.activity_main);

          anhxa();

          xulychatbot();
          xulynutchat();
          //takephoto();
          //chonhinh();
        //  dodulieuvaolist();
        }

    private void xulychatbot() {

        arrchat.add(new Chatitem(R.drawable.avatabot,"Chào bạn! Bạn cần giúp gì? Chuyên môn của mình là cung cấp một số gợi ý về du lịch như địa điểm, món ăn, hoạt động vui chơi, ... ở Bến Tre, Cần Thơ và Đà Lạt"));
        dongchatbotadapter = new dongchatbotadapter(MainActivity.this,R.layout.dong_chatbot,arrchat);
        listchat.setAdapter(dongchatbotadapter);


    }

    // chuc nang chua phat trien xong
 /*   private void dodulieuvaolist() {
        byte[] anh = imgtobyte(hinhchat);
        arrhinhchat.add(new HInhitem(R.drawable.canboavata,anh));
        dongchathinhdapter = new dongchathinhdapter(MainActivity.this,R.layout.dong_chathinh,arrhinhchat);
        listchat.setAdapter(dongchathinhdapter);
    }

    private void chonhinh() {
        guihinh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2 = new Intent(Intent.ACTION_PICK);
                intent2.setType("image/*");
                startActivityForResult(intent2,321);
                }
        });
    }

    private void takephoto() {
        nutchupanh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(intent,123);



            }
        });
    }
*/
    private void xulynutchat() {

        nutchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lanoidungchat = noidungchatedittext.getText().toString();
                //su kien chon du lich
                if(lanoidungchat.equals("mình muốn đi du lịch ở cần thơ"))
                {   arrchat.add(new Chatitem(R.drawable.canboavata,lanoidungchat));
                    arrchat.add(new Chatitem(R.drawable.avatabot,"Bạn muốn tìm thông tin về cần thơ vui lòng gõ chi tiết"));
                    dongchatbotadapter = new dongchatbotadapter(MainActivity.this,R.layout.dong_chatbot,arrchat);
                    listchat.setAdapter(dongchatbotadapter);
                    noidungchatedittext.setText(null);
                }
                else if (lanoidungchat.equals("chi tiết"))
                {
                    Intent intent = new Intent(MainActivity.this,goiycantho.class);
                    startActivity(intent);

                }
                    else
                noidungchatedittext.setText(null);

            }
        });

    }
// chuc nang chua phat trien xong
  /*  @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
      if (requestCode == 321)
      {
          try
          {
              Uri imageUri = data.getData();
              InputStream is = getContentResolver().openInputStream(imageUri);
              Bitmap bitmap = BitmapFactory.decodeStream(is);
              hinhchat.setImageBitmap(bitmap);
          } catch (FileNotFoundException e) {
              e.printStackTrace();
          }
      } else if (requestCode == 123 )
      {
          Bitmap bitmap = (Bitmap) data.getExtras().get("data");
          hinhchat.setImageBitmap(bitmap);
      }

    } */
// doi hinh sang mang byte

  /*  private byte[] imgtobyte (ImageView img)
    {
        BitmapDrawable drawable = (BitmapDrawable) img.getDrawable();
        Bitmap bmp = drawable.getBitmap();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] bytearray = stream.toByteArray();
        return bytearray;


    } */

    private void anhxa() {
        listchat = findViewById(R.id.listkhungchat);
        arrchat = new ArrayList<>();
        nutchat = findViewById(R.id.iconsend);
        noidungchatedittext = findViewById(R.id.textchat);
        guihinh = findViewById(R.id.setphoto);
        nutchupanh = findViewById(R.id.takephoto);
        guilink = findViewById(R.id.setlink);
        arrchatbot = new ArrayList<>();
        arrchat = new ArrayList<>();


    }

}


