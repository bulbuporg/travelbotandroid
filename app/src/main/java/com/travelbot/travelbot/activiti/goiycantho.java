package com.travelbot.travelbot.activiti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.adapter.dongchatadapter;
import com.travelbot.travelbot.mode.HInhitem;

import java.util.ArrayList;

public class goiycantho extends AppCompatActivity {
    ArrayList<HInhitem> arrhinhchat;
    ListView listgoiy;
    dongchatadapter dongchatadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_goiycantho);


        arrhinhchat = new ArrayList<>();
        // them listview goi y cho bot
        arrhinhchat.add(new HInhitem(R.drawable.anhmonangoiy,"Món ăn","Thông tin về các món ăn ở cần thơ"));
        arrhinhchat.add(new HInhitem(R.drawable.diaiemvuichoi,"Địa điểm vui chơi","Thông tin về các địa điểm vui chơi"));
        arrhinhchat.add(new HInhitem(R.drawable.nhahanggoiy,"Nhà hàng","Thông tin về các nhà hàng ở cần thơ"));
        arrhinhchat.add(new HInhitem(R.drawable.khachsangoiy,"Khách sạn","Thông tin về các khách sạn ở cần thơ"));
        arrhinhchat.add(new HInhitem(R.drawable.qualuuniem,"Quà tặng","Thông tin về quà tặng nên mua về"));
        dongchatadapter = new dongchatadapter(this,R.layout.dong_chathinh,arrhinhchat);

        listgoiy = findViewById(R.id.listgoiy);
        listgoiy.setAdapter(dongchatadapter);


        // code xu ly su kien click


        listgoiy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                switch (i)
                {
                    case 0:
                        Intent intent = new Intent(goiycantho.this,monan.class);
                        startActivity(intent);
                }
            }
        });
    }
}
