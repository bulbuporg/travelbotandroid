package com.travelbot.travelbot.activiti;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.adapter.dongchatadapter;
import com.travelbot.travelbot.mode.HInhitem;

import java.util.ArrayList;

public class monan extends AppCompatActivity {
    ArrayList<HInhitem> arrhinhchat;
    ListView listgoiy;
  dongchatadapter dongchatadapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monan);

        arrhinhchat = new ArrayList<>();
        // them listview goi y cho bot
        arrhinhchat.add(new HInhitem(R.drawable.laumamdally,"Lẩu mắm dạ lý","Địa chỉ: 89 Đường 3/2,Quận ninh kiều thành phố Cần Thơ"));
        arrhinhchat.add(new HInhitem(R.drawable.vitnauchao,"Vịt nấu chao","Địa chỉ: hẻm 1 đường Lý Tự Trọng Cần Thơ"));
        arrhinhchat.add(new HInhitem(R.drawable.nemnuongthanhvan,"Nem nướng thanh vân","17 Đại lộ hòa bình, quận ninh kiều thành phố Cần Thơ"));
        arrhinhchat.add(new HInhitem(R.drawable.banhxeo,"Bánh xèo","Dường cái sơn hàng bàng quận cái răng Cần thơ"));

        dongchatadapter = new dongchatadapter(this,R.layout.dong_chathinh,arrhinhchat);

        listgoiy = findViewById(R.id.listgoiymonan);
        listgoiy.setAdapter(dongchatadapter);







    }
}
