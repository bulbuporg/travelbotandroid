package com.travelbot.travelbot.activiti;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.adapter.AdapterDiaChi;
import com.travelbot.travelbot.adapter.AdapterDiaDiem;
import com.travelbot.travelbot.adapter.AdapterReview;
import com.travelbot.travelbot.mode.DiaChiButton;
import com.travelbot.travelbot.mode.DiaDiem;
import com.travelbot.travelbot.mode.Review;

import java.util.ArrayList;

public class ChiTietMonAn extends AppCompatActivity {

    ViewFlipper vfpMonAn;
    float toadoX1,toadoX2;

    AdapterDiaDiem adapterDiaDiem;
    ArrayList<DiaDiem> arrDiaDiem;
    ListView lvDiaDiem;

    ListView lvReview;
    ArrayList<Review> arrReview;
    AdapterReview adapterReview;

    GridView gvDiaChi;
    AdapterDiaChi adapterDiaChi;
    ArrayList<DiaChiButton> arrDiaChiButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chi_tiet_mon_an);

        addControl();
        addEvent();
        vfpMonAn.setInAnimation(ChiTietMonAn.this,android.R.anim.fade_in);
        vfpMonAn.setOutAnimation(ChiTietMonAn.this,android.R.anim.fade_out);
        vfpMonAn.startFlipping();
        vfpMonAn.setFlipInterval(2000);
    }

    private void addEvent() {

    }

    private void addControl() {
        vfpMonAn = (ViewFlipper) findViewById(R.id.vfpMonAn);
        lvDiaDiem = (ListView) findViewById(R.id.lvDiaDiem);
        arrDiaDiem = new ArrayList<>();
        arrDiaDiem.add(new DiaDiem("Bánh xèo A Mật",1.2f,R.drawable.banhxeo1,"0123456789","Phường Nhà Mát,TP Bạc Liêu"));
        arrDiaDiem.add(new DiaDiem("Bánh xèo Quang Trung",1.2f,R.drawable.banhxeo2,"0946456123","Quận Cái Răng,TP Cần Thơ"));
        arrDiaDiem.add(new DiaDiem("Bánh xèo QUÊ HƯƠNG",1.2f,R.drawable.banhxeo3,"0123521201","Phường 8,TP Bạc liêu"));
        arrDiaDiem.add(new DiaDiem("Bánh xèo Chi Chi",1.2f,R.drawable.banhxeo4,"0123456789","Phường 3,TP Sóc Trăng"));
        adapterDiaDiem = new AdapterDiaDiem(ChiTietMonAn.this,R.layout.diadiem_item,arrDiaDiem);
        lvDiaDiem.setAdapter(adapterDiaDiem);

        lvReview = (ListView) findViewById(R.id.lvReview);
        arrReview = new ArrayList<>();
        arrReview.add(new Review(R.drawable.reviewer1,"Tôi đã đến đây và trải nghiệm món ăn,bánh xèo rất ngon và giá cả hợp lý"));
        arrReview.add(new Review(R.drawable.reviewer2,"Bánh xèo rất ngon,có dịp tôi sẽ tiếp tục đến đây,nơi đây không gian thoải mái"));
        arrReview.add(new Review(R.drawable.reviewer3,"Nhân viên phục vụ rất chu đáo,không gian rộng rãi,thích hợp ăn gia đình"));
        adapterReview = new AdapterReview(ChiTietMonAn.this,R.layout.layout_review,arrReview);
        lvReview.setAdapter(adapterReview);

        gvDiaChi = (GridView) findViewById(R.id.gvDiaChi);
        arrDiaChiButton = new ArrayList<>();
        arrDiaChiButton.add(new DiaChiButton(android.R.drawable.ic_menu_call,"Call","0946456456"));
        arrDiaChiButton.add(new DiaChiButton(android.R.drawable.ic_menu_directions,"Directions",""));
        adapterDiaChi = new AdapterDiaChi(ChiTietMonAn.this,R.layout.diachi_layout_gridview,arrDiaChiButton);
        gvDiaChi.setAdapter(adapterDiaChi);
    }




}

