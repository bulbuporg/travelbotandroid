package com.travelbot.travelbot.activiti;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.travelbot.travelbot.R;
import com.travelbot.travelbot.adapter.object_item;
import com.travelbot.travelbot.adapter.Adapter_RecycleView;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by EviL NTB on 12/24/2017.
 */

public class chititet extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chitiet);
        ImageView imageView = (ImageView) findViewById(R.id.img_chitiet) ;
        Picasso.with(this).load("https://travel.com.vn/Images/destination/dc_150506_images%20(3).jpg").fit().into(imageView);

        List<object_item> list_item = new ArrayList<>();
        list_item.add(new object_item("http://www.dulichmientayct.com/images/imgs/images/dem-tren-ben-ninh-kieu.jpg","Bến Ninh Kiều"));
        list_item.add(new object_item("https://www.dulichvietnam.com.vn/data/chonoi1.jpg","Chợ Nỗi Cần Thơ"));
        list_item.add(new object_item("http://sohanews.sohacdn.com/thumb_w/660/2017/photo1497166529849-1497166530006-0-0-409-660-crop-1497166563617.jpg","Cầu Cần Thơ"));
        list_item.add(new object_item("http://www.dulichmientayct.com/images/imgs/images/dem-tren-ben-ninh-kieu.jpg","Bến Ninh Kiều"));
        list_item.add(new object_item("https://www.dulichvietnam.com.vn/data/chonoi1.jpg","Chợ Nỗi Cần Thơ"));
        list_item.add(new object_item("http://sohanews.sohacdn.com/thumb_w/660/2017/photo1497166529849-1497166530006-0-0-409-660-crop-1497166563617.jpg","Cầu Cần Thơ"));

        RecyclerView item = (RecyclerView) findViewById(R.id.rc_item);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        item.setLayoutManager(horizontalLayoutManagaer);
        Adapter_RecycleView adapter_recycleView = new Adapter_RecycleView(list_item,this);
        item.setAdapter(adapter_recycleView);
    }
}
