package com.travelbot.travelbot.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.mode.Chatitem;

import java.util.List;

/**
 * Created by Admin on 12/22/2017.
 */

public class dongchatbotadapter extends BaseAdapter {

    Context context;
    private int layout;
    private List<Chatitem> menulist;

    public dongchatbotadapter(Context context, int layout, List<Chatitem> menulist) {
        this.context = context;
        this.layout = layout;
        this.menulist = menulist;
    }

    @Override
    public int getCount() {
        return menulist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    private class viewholdermenu
    {
        ImageView hinhavata;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        dongchatbotadapter.viewholdermenu holder;
        if(view == null)
        {
            holder = new dongchatbotadapter.viewholdermenu();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout,null);
            //anh xa view
            holder.hinhavata =(ImageView) view.findViewById(R.id.hinhavatabot);

            view.setTag(holder);

        }
        else
        {
            holder = (dongchatbotadapter.viewholdermenu) view.getTag();
        }
        //anh xa text view
        TextView txtchat = (TextView) view.findViewById(R.id.dongchatbot);
        //gan gia tri
        Chatitem chatitem = menulist.get(i);
        txtchat.setText(chatitem.getNoidungchat());

        holder.hinhavata.setImageResource(chatitem.getAvata());


        return view;
    }
}
