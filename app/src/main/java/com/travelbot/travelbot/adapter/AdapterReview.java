package com.travelbot.travelbot.adapter;

/**
 * Created by Dell on 12/23/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.mode.Review;

import java.util.List;

/**
 * Created by Dell on 12/22/2017.
 */

public class AdapterReview extends ArrayAdapter<Review> {
    Activity context;
    int resource;
    List<Review> objects;
    public AdapterReview(@NonNull Activity context, @LayoutRes int resource, @NonNull List<Review> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View row = inflater.inflate(this.resource,null);

        ImageView imgReviewer = (ImageView) row.findViewById(R.id.imgReviewer);
        TextView txtNoiDungRV = (TextView) row.findViewById(R.id.txtNoiDungRV);

        Review review = this.objects.get(position);
        imgReviewer.setImageResource(review.getImgReview());
        txtNoiDungRV.setText(review.getNoiDung());
        return row;
    }
}
