package com.travelbot.travelbot.adapter;

/**
 * Created by EviL NTB on 12/24/2017.
 */

public class object_item {
    String url_img;
    String title;

    public object_item(String url_img, String title) {
        this.url_img = url_img;
        this.title = title;
    }

    public String getUrl_img() {
        return url_img;
    }

    public void setUrl_img(String url_img) {
        this.url_img = url_img;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
