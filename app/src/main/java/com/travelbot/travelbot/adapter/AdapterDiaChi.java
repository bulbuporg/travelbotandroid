package com.travelbot.travelbot.adapter;

/**
 * Created by Dell on 12/23/2017.
 */

import android.app.Activity;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.mode.DiaChiButton;

import java.util.List;

/**
 * Created by Dell on 12/22/2017.
 */

public class AdapterDiaChi extends ArrayAdapter<DiaChiButton> {
    Activity context;
    int resource;
    List<DiaChiButton> objects;
    public AdapterDiaChi(@NonNull Activity context, @LayoutRes int resource, @NonNull List<DiaChiButton> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View row = inflater.inflate(this.resource,null);

        ImageView imgDiaChiButton = (ImageView) row.findViewById(R.id.imgButtonDiaChi);
        TextView txtTenButton = (TextView) row.findViewById(R.id.txtTenButton);
        TextView txtPhoneButton = (TextView) row.findViewById(R.id.txtPhoneButton);

        DiaChiButton diaChiButton =  this.objects.get(position);
        imgDiaChiButton.setImageResource(diaChiButton.getImgButton());
        txtTenButton.setText(diaChiButton.getTenButton());
        txtPhoneButton.setText(diaChiButton.getPhoneButton());

        return row;
    }
}

