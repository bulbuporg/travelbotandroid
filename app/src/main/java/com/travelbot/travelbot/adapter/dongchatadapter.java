package com.travelbot.travelbot.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.travelbot.travelbot.R;

import com.travelbot.travelbot.mode.HInhitem;

import java.util.List;

/**
 * Created by Admin on 12/18/2017.
 */

public class dongchatadapter extends BaseAdapter {

    Context context;
    private int layout;
    private List<HInhitem> menulist;


    public dongchatadapter(Context context, int layout, List<HInhitem> menulist) {
        this.context = context;
        this.layout = layout;
        this.menulist = menulist;
    }

    @Override
    public int getCount() {
        return menulist.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }


    private class viewholdermenu
    {
        ImageView hinhgoiy;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup)
    {
        dongchatadapter.viewholdermenu holder;
        if(view == null)
        {
            holder = new dongchatadapter.viewholdermenu();
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(layout,null);
            //anh xa view
            holder.hinhgoiy =(ImageView) view.findViewById(R.id.anhgoiy);
            view.setTag(holder);

        }
        else
        {
            holder = (dongchatadapter.viewholdermenu) view.getTag();
        }
        //anh xa text view
        TextView txtchat = (TextView) view.findViewById(R.id.tieude);
        TextView txtchat2 = (TextView) view.findViewById(R.id.mota);
        //gan gia tri
        HInhitem hinhitem = menulist.get(i);
        txtchat.setText(hinhitem.getTieude());
        txtchat2.setText(hinhitem.getMota());

        holder.hinhgoiy.setImageResource(hinhitem.getAnhgoiy());
        return view;
    }
}
