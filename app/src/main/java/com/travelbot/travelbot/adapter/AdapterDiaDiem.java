package com.travelbot.travelbot.adapter;

/**
 * Created by Dell on 12/23/2017.
 */

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.travelbot.travelbot.R;
import com.travelbot.travelbot.mode.DiaDiem;

import java.util.List;

/**
 * Created by Dell on 12/20/2017.
 */

public class AdapterDiaDiem extends ArrayAdapter<DiaDiem> {

    Activity context;
    int resource;
    List<DiaDiem> objects;

    public AdapterDiaDiem(@NonNull Activity context, @LayoutRes int resource, @NonNull List<DiaDiem> objects) {
        super(context, resource, objects);
        this.context = context;
        this.resource = resource;
        this.objects = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = this.context.getLayoutInflater();
        View row = inflater.inflate(this.resource,null);

        ImageView imgDiaDiem = (ImageView) row.findViewById(R.id.imgDiaDiem);
        TextView txtTenDiaDiem = (TextView) row.findViewById(R.id.txtTenDiaDiem);
        //RatingBar rtbDiaDiem = (RatingBar) row.findViewById(R.id.rabDiaDiem);
        TextView txtPhoneDiaDiem = (TextView) row.findViewById(R.id.txtPhoneDiaDiem);
        TextView txtDiaChi = (TextView) row.findViewById(R.id.txtDiaChiDiaDiem);

        DiaDiem diaDiem = this.objects.get(position);
        txtTenDiaDiem.setText((position+1)+"." + diaDiem.getTenDiaDiem());
        txtPhoneDiaDiem.setText(diaDiem.getPhone());
        txtDiaChi.setText(diaDiem.getDiaChi());
        imgDiaDiem.setImageResource(diaDiem.getImgDiaDiem());
        return row;
    }
}

