package com.travelbot.travelbot.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;
import com.travelbot.travelbot.R;
/**
 * Created by EviL NTB on 12/24/2017.
 */

public class Adapter_RecycleView extends RecyclerView.Adapter<Adapter_RecycleView.ViewHolder>{


    List<object_item> object_items = new ArrayList<>();
    Context context;
    private LayoutInflater mInflater;

    public Adapter_RecycleView(List<object_item> object_items, Context context) {
        this.object_items = object_items;
        this.context = context;
        this.mInflater = LayoutInflater.from(context);

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(Adapter_RecycleView.ViewHolder holder, int position) {
        holder.myTextView.setText(object_items.get(position).getTitle());
        Picasso.with(context).load(object_items.get(position).getUrl_img()).fit().into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return object_items.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder  {
        public ImageView imageView;
        public TextView myTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.img_item);
            myTextView = itemView.findViewById(R.id.tv_title_item);

        }

    }

}
