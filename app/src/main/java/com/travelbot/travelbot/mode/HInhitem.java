package com.travelbot.travelbot.mode;

import android.widget.ImageView;

/**
 * Created by Admin on 12/20/2017.
 */

public class HInhitem {
    private int anhgoiy;
    private String tieude;
    private String mota;

    public HInhitem(int anhgoiy, String tieude, String mota) {
        this.anhgoiy = anhgoiy;
        this.tieude = tieude;
        this.mota = mota;
    }

    public int getAnhgoiy() {
        return anhgoiy;
    }

    public void setAnhgoiy(int anhgoiy) {
        this.anhgoiy = anhgoiy;
    }

    public String getTieude() {
        return tieude;
    }

    public void setTieude(String tieude) {
        this.tieude = tieude;
    }

    public String getMota() {
        return mota;
    }

    public void setMota(String mota) {
        this.mota = mota;
    }
}
