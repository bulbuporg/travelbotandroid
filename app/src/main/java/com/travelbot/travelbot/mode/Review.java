package com.travelbot.travelbot.mode;

/**
 * Created by Dell on 12/23/2017.
 */

public class Review {
    int imgReview;
    String noiDung;

    public Review(int imgReview, String noiDung) {
        this.imgReview = imgReview;
        this.noiDung = noiDung;
    }

    public Review() {
    }

    public int getImgReview() {
        return imgReview;
    }

    public void setImgReview(int imgReview) {
        this.imgReview = imgReview;
    }

    public String getNoiDung() {
        return noiDung;
    }

    public void setNoiDung(String noiDung) {
        this.noiDung = noiDung;
    }
}
