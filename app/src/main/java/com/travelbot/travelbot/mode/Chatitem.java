package com.travelbot.travelbot.mode;

import android.widget.ListView;

/**
 * Created by Admin on 12/19/2017.
 */

public class Chatitem {
    public int avata;
    private String noidungchat;



    public Chatitem(int avata, String noidungchat) {
        this.avata = avata;
        this.noidungchat = noidungchat;
    }

    public int getAvata() {
        return avata;
    }

    public void setAvata(int avata) {
        this.avata = avata;
    }

    public String getNoidungchat() {
        return noidungchat;
    }

    public void setNoidungchat(String noidungchat) {
        this.noidungchat = noidungchat;
    }
}
