package com.travelbot.travelbot.mode;

/**
 * Created by Dell on 12/23/2017.
 */

public class DiaChiButton {
    int imgButton;
    String tenButton;
    String phoneButton;

    public DiaChiButton(int imgButton, String tenButton, String phoneButton) {
        this.imgButton = imgButton;
        this.tenButton = tenButton;
        this.phoneButton = phoneButton;
    }

    public DiaChiButton() {
    }

    public int getImgButton() {
        return imgButton;
    }

    public void setImgButton(int imgButton) {
        this.imgButton = imgButton;
    }

    public String getTenButton() {
        return tenButton;
    }

    public void setTenButton(String tenButton) {
        this.tenButton = tenButton;
    }

    public String getPhoneButton() {
        return phoneButton;
    }

    public void setPhoneButton(String phoneButton) {
        this.phoneButton = phoneButton;
    }
}
