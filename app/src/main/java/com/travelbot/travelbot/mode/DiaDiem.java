package com.travelbot.travelbot.mode;

/**
 * Created by Dell on 12/22/2017.
 */

public class DiaDiem {
    String tenDiaDiem;
    float diemDanhGia;
    int imgDiaDiem;
    String phone;
    String diaChi;

    public String getTenDiaDiem() {
        return tenDiaDiem;
    }

    public void setTenDiaDiem(String tenDiaDiem) {
        this.tenDiaDiem = tenDiaDiem;
    }

    public float getDiemDanhGia() {
        return diemDanhGia;
    }

    public void setDiemDanhGia(float diemDanhGia) {
        this.diemDanhGia = diemDanhGia;
    }

    public int getImgDiaDiem() {
        return imgDiaDiem;
    }

    public void setImgDiaDiem(int imgDiaDiem) {
        this.imgDiaDiem = imgDiaDiem;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public DiaDiem() {

    }

    public DiaDiem(String tenDiaDiem, float diemDanhGia, int imgDiaDiem, String phone, String diaChi) {

        this.tenDiaDiem = tenDiaDiem;
        this.diemDanhGia = diemDanhGia;
        this.imgDiaDiem = imgDiaDiem;
        this.phone = phone;
        this.diaChi = diaChi;
    }
}
